from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Wallet
from .models import CustomUser


class TransferTest(APITestCase):
    def test_transfer(self):
        user1 = CustomUser.objects.create(email='a@mail.ru', password='Parish022', is_active=True, is_staff=True)
        user2 = CustomUser.objects.create(email='b@mail.ru', password='Parish022', is_active=True, is_staff=True)
        wallet1 = Wallet.objects.create(balance=50, currency='RUB', owner_id=user1)
        wallet2 = Wallet.objects.create(balance=50, currency='USD', owner_id=user2)
        url = reverse('send_money')
        self.client.force_authenticate(user2)
        data = {'pk': 1, 'amount': 10}
        response = self.client.post(url, data)
        wallet = Wallet.objects.values_list('balance', flat=True)
        self.assertEqual([balance for balance in wallet], [650, 40])
        self.assertEqual(status.HTTP_200_OK, response.status_code)