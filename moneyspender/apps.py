from django.apps import AppConfig


class MoneyspenderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'moneyspender'
