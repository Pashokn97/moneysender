from moneyspender.manager import CustomUserManager


from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models


class Currency(models.TextChoices):
    RUB = 'RUB', 'рубль'
    USD = 'USD', 'долар'
    EUR = 'EUR', 'евро'


cuorse = {
    Currency.RUB:  1/60,
    Currency.USD: 1,
    Currency.EUR: 1.2,
}


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(('email address'), unique=True)
    password = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Operation(models.Model):
    value = models.CharField(max_length=100)
    receiver = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='receiver_operations')
    sender = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='sender_operations')


class OperationType(models.TextChoices):
    send = 'Операция отправления'
    receive = 'Операция получения'


class Wallet(models.Model):
    balance = models.IntegerField(blank=True, default=10)
    currency = models.CharField(choices=Currency.choices, max_length=50, default=Currency.RUB)
    owner_id = models.ForeignKey('CustomUser', on_delete=models.CASCADE)