from rest_framework import generics
from rest_framework.generics import UpdateAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response


from .serializers import *
from .servise import *


class CreateUser(generics.CreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UsersSerializers
    permission_classes = (AllowAny,)


class UserList(generics.ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UsersSerializers


class SendMoney(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TransferSeializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sender = self.request.user
        receiver = serializer.validated_data['pk']
        amount = serializer.validated_data['amount']
        transfer(sender, receiver, amount)

        return Response()


class OperationsList(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = MyOperations
    queryset = Operation.objects.all()

    def get_queryset(self):
        return self.queryset.filter(sender=self.request.user.pk) | self.queryset.filter(receiver=self.request.user.pk)


class CreateWallet(GenericAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = WalletSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        wallet = Wallet.objects.create(
            balance=serializer.validated_data['balance'],
            currency=serializer.validated_data['currency'],
            owner_id=self.request.user
        )
        wallet.save()
        return Response()
