from rest_framework import serializers
from .models import *


class UsersSerializers(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ('email', 'password')

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = CustomUser.objects.create(**validated_data)
        user.set_password(password)
        user.save()
        return user


class TransferSeializer(serializers.Serializer):
    pk = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())
    amount = serializers.IntegerField()


class MyOperations(serializers.ModelSerializer):
    operation_type = serializers.SerializerMethodField()

    class Meta:
        model = Operation
        fields = ('receiver', 'value', 'sender', 'operation_type')

    def get_operation_type(self, obj):
        if self.context['request'].user == obj.sender:
            return OperationType.send
        else:
            return OperationType.receive


class UserSerializer(serializers.ModelSerializer):
    class Meta:

        model = CustomUser
        fields = '__all__'


class WalletSerializer(serializers.ModelSerializer):
    owner_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Wallet
        fields = ('balance', 'currency', 'owner_id')

