from rest_framework.exceptions import ValidationError

from .models import cuorse, Operation, Wallet


def log_operation(func):
    def wrapper(sender, receiver, amount):
        func(sender, receiver, amount)
        Operation.objects.create(value=amount, receiver=receiver, sender=sender)
    return wrapper


@log_operation
def transfer(sender, receiver, amount):
    sender_wallet=Wallet.objects.get(owner_id=sender.pk)
    receiver_wallet = Wallet.objects.get(owner_id=receiver.pk)
    if sender_wallet.balance >= amount:
        sender_wallet.balance = sender_wallet.balance - amount

        sender_amount_in_dollars = amount * cuorse.get(sender_wallet.currency)
        receiver_wallet.balance += sender_amount_in_dollars / cuorse.get(receiver_wallet.currency)

        sender_wallet.save()
        receiver_wallet.save()
    else:
        raise ValidationError('Недостаточно средств')


