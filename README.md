###The project is REST API, allow use following functional:
    1. Register a user;
    2. Create wallet;
    3. Make money transfer with different currencies;
    4. Receive list of your operations;


###Necessary for launch:
    1. Python==3.7
    2. Django==4.1.3
    3. djangorestframework==3.14.0
    4. sqlparse==0.4.3

###Installing:
    
    1. Clone repository: 
        git clone https://gitlab.com/Pashokn97/moneysender
    2. Create virtual environment and activate it:
        python3 -m venv venv
        source venv/bin/activate
    3. Install requirements:
        pip install -r requirements.txt
    4. Make migrations:
        python manage.py migrate
    5. Run server:
        python manage.py runserver
    
###Run tests:
    python manage.py test
